package com.infobip.urlshortener.dao;

import com.infobip.urlshortener.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class AccountDao {
    @Autowired private NamedParameterJdbcTemplate jdbcTemplate;

    public Account create(Account account){
        String sql = "INSERT INTO accounts" +
                " (accountId, password)" +
                " VALUES" +
                " (?,?)";
        jdbcTemplate.getJdbcOperations().update(sql,
                account.getAccountId(),
                account.getPassword());
        return account;
    }

    public Optional<Account> findByAccountId(String accountId) {
        String sql = "SELECT * FROM accounts WHERE accountId=?";
        List<Account> result = jdbcTemplate.getJdbcOperations().query(sql,accountDataRowMapper, accountId);
        return result.isEmpty()? Optional.empty() : Optional.of(result.get(0));
    }


    private final RowMapper<Account> accountDataRowMapper =
            (rowSet, rowNum) ->
            {
                Account account = new Account(rowSet.getString("accountId"),
                        rowSet.getString("password"));
                return account;
            };
}

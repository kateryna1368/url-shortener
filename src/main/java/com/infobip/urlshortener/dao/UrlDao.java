package com.infobip.urlshortener.dao;

import com.infobip.urlshortener.model.Url;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class UrlDao {
    @Autowired private NamedParameterJdbcTemplate jdbcTemplate;

    public Url create(Url url){
        String sql = "INSERT INTO urls" +
                " (url, shortUrl, accountId, redirectCount, redirectCode)" +
                " VALUES" +
                " (?,?,?,?,?)";
        jdbcTemplate.getJdbcOperations().update(sql,
                url.getUrl(),
                url.getShortUrl(),
                url.getAccountId(),
                url.getRedirectCount(),
                url.getRedirectCode());
        return url;
    }

    public Url updateCount(Url url){
        String sql = "update urls" +
                " set redirectCount=?"  +
                " where shortUrl=?";
        jdbcTemplate.getJdbcOperations().update(sql,
                url.getRedirectCount()+1,
                url.getShortUrl());
        return url;
    }

    public Optional<Url> findByShortUrl(String shortUrl) {
        String sql = "SELECT * FROM urls WHERE shortUrl=?";

        List<Url> result = jdbcTemplate.getJdbcOperations().query(
                sql,
                urlDataRowMapper,
                shortUrl);
        return result.isEmpty()? Optional.empty() : Optional.of(result.get(0));
    }

    public List<Url> findByAccountId(String accountId) {
        String sql = "SELECT * FROM urls WHERE accountId=?";
        List<Url> result = jdbcTemplate.getJdbcOperations().query(sql,urlDataRowMapper, accountId);
        return result;

    }

    private final RowMapper<Url> urlDataRowMapper =
            (rowSet, rowNum) ->
            {
                Url user = new Url(rowSet.getString("url"),
                        rowSet.getString("shortUrl"),rowSet.getInt("redirectCount"),
                        rowSet.getString("accountId"),rowSet.getInt("redirectCode"));
                return user;
            };
}

package com.infobip.urlshortener.model;


public class Url {
    private String url;

    private String shortUrl;

    private Integer redirectCount;

    private String accountId;

    public Url(String url, String shortUrl, Integer redirectCount, String accountId, Integer redirectCode) {
        this.url = url;
        this.shortUrl = shortUrl;
        this.redirectCount = redirectCount;
        this.accountId = accountId;
        this.redirectCode = redirectCode;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    public Integer getRedirectCount() {
        return redirectCount;
    }

    public void setRedirectCount(Integer redirectCount) {
        this.redirectCount = redirectCount;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public Integer getRedirectCode() {
        return redirectCode;
    }

    public void setRedirectCode(Integer redirectCode) {
        this.redirectCode = redirectCode;
    }

    private Integer redirectCode;

}

package com.infobip.urlshortener.service;

import com.infobip.urlshortener.dao.UrlDao;
import com.infobip.urlshortener.model.Url;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UrlShortenerService {
    @Autowired private UrlDao urlDao;

    public String register(String uri, Integer redirectType, String accountId) {
        String shortUrl = "http://localhost:8080/r/" + RandomStringUtils.randomAlphabetic(6);
        Url url = new Url(uri, shortUrl, 0, accountId, redirectType != null ? redirectType : 302);
        urlDao.create(url);
        return shortUrl;
    }

    public void redirectCountInc(Url url){
        urlDao.updateCount(url);
    }

    public Map<String, Integer> getStatisticByAccountId(String accountId){
        List<Url> dataList = urlDao.findByAccountId(accountId);
        return dataList.stream()
                .collect(Collectors.toMap(d -> d.getUrl(), d -> d.getRedirectCount(), (p1, p2) -> p1+p2));
    }

    public Optional<Url> findByShortUrl(String shortUrl){
        return urlDao.findByShortUrl(shortUrl);
    }
}

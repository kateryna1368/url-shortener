package com.infobip.urlshortener.service;

import com.infobip.urlshortener.dao.AccountDao;
import com.infobip.urlshortener.model.Account;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Optional;

@Service
public class AccountService implements UserDetailsService {
    @Autowired private AccountDao accountDao;
    @Autowired private PasswordEncoder passwordEncoder;

    public Optional<String> create(String id) {
        if(accountDao.findByAccountId(id).isPresent()){
            return Optional.empty();
        }
        String password = RandomStringUtils.randomAlphanumeric(8);
        Account account = new Account(id, passwordEncoder.encode(password));
        accountDao.create(account);
        return Optional.of(password);
    }

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Account> accountData = accountDao.findByAccountId(username);
        if(accountData.isPresent()){
            return new User(username, accountData.get().getPassword(), true, true, true,
                    true, Arrays.asList(new SimpleGrantedAuthority("USER")) );
        }
        throw new UsernameNotFoundException("Account '" + username + "' not found");
    }

}

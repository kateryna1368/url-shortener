package com.infobip.urlshortener.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication(scanBasePackages={"com.infobip.urlshortener"})
public class UrlShortenerApp extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(UrlShortenerApp.class);
    }
    public static void main(String[] args) {
        SpringApplication.run(UrlShortenerApp.class, args);
    }
}
package com.infobip.urlshortener.controller;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.infobip.urlshortener.service.AccountService;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.infobip.urlshortener.controller.ConstantsHolder.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Controller
public class AccountController {
    @Autowired private AccountService accountService;

    @RequestMapping("/account")
    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity account(@Valid @RequestBody AccountRequest request) {
        Optional<String> result = accountService.create(request.accountId);

        Map<String, Object> response = new HashMap<>();
        boolean isAccountCreated = result.isPresent();
        response.put(SUCCESS, isAccountCreated);

        if(!isAccountCreated){
            response.put(DESCRIPTION,"Account with that ID already exists");
            return new ResponseEntity<>(response, HttpStatus.CONFLICT);
        }

        response.put(DESCRIPTION,"Your account is opened");
        response.put("password", result.get());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Map> badRequestHandler(Exception ex) {
        Map<String, Object> responseBody = new HashMap<>();
        responseBody.put(SUCCESS, false);
        responseBody.put(DESCRIPTION, "Invalid argument");
        return new ResponseEntity<>(responseBody, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Map> internalErrorHandler(Exception ex) {
        Map<String, Object> responseBody = new HashMap<>();
        responseBody.put(SUCCESS, false);
            responseBody.put(DESCRIPTION, INTERNAL_SERVER_ERROR);
            return new ResponseEntity<>(responseBody, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private static class AccountRequest{
        @NotBlank
        @JsonProperty("AccountId")
        private String accountId;
    }

}

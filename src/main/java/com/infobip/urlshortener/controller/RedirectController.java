package com.infobip.urlshortener.controller;

import com.infobip.urlshortener.model.Url;
import com.infobip.urlshortener.service.UrlShortenerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.infobip.urlshortener.controller.ConstantsHolder.*;

@Controller
@RequestMapping(path = "/r")
public class RedirectController {
    @Autowired private UrlShortenerService urlShortenerService;

    @GetMapping(path = "/**")
    public void redirectUrl(HttpServletRequest request,  HttpServletResponse response) throws IOException {
        String shortUrl = request.getRequestURL().toString();
        Optional<Url> url = urlShortenerService.findByShortUrl(shortUrl);
        if (!url.isPresent()) {
            response.sendError(404, "URL not found");
        }
        Url urlData = url.get();
        urlShortenerService.redirectCountInc(urlData);
        response.setStatus(urlData.getRedirectCode());
        response.sendRedirect(urlData.getUrl());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Map> internalErrorHandler(Exception ex) {
        Map<String, Object> responseBody = new HashMap<>();
        responseBody.put(SUCCESS, false);
        responseBody.put(DESCRIPTION, INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(responseBody, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}

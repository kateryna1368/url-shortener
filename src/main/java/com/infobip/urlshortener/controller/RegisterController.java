package com.infobip.urlshortener.controller;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.infobip.urlshortener.service.UrlShortenerService;
import org.apache.commons.validator.routines.UrlValidator;
import org.hibernate.validator.constraints.URL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import static com.infobip.urlshortener.controller.ConstantsHolder.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Controller
@RequestMapping(path = "/register")
public class RegisterController {
    @Autowired private UrlShortenerService urlShortenerService;

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Map> register(@Valid @RequestBody URLRegisterRequest request,
            @AuthenticationPrincipal UserDetails userDetails)
            throws URISyntaxException {
        Map<String, Object> responseBody = new HashMap<>();
        if (!new UrlValidator().isValid(request.url)) {
            return processInvalidUrl();
        }

        String shortUrl = urlShortenerService.register(request.url, request.redirectType, userDetails.getUsername());
        responseBody.put(SUCCESS, true);
        responseBody.put("shortUrl", shortUrl);
        return new ResponseEntity<>(responseBody, HttpStatus.OK);
    }

    @ExceptionHandler(URISyntaxException.class)
    public ResponseEntity<Map> internalErrorHandler(URISyntaxException ex) {
        return processInvalidUrl();
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Map> internalErrorHandler(Exception ex) {
        Map<String, Object> responseBody = new HashMap<>();
        responseBody.put(SUCCESS, false);
        responseBody.put(DESCRIPTION, INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(responseBody, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<Map> processInvalidUrl() {
        Map<String, Object> responseBody = new HashMap<>();
        responseBody.put(SUCCESS, false);
        responseBody.put(DESCRIPTION, "URL is invalid");
        return new ResponseEntity<>(responseBody, HttpStatus.BAD_REQUEST);
    }

    private static class URLRegisterRequest {
        @URL
        @JsonProperty(value = "url", required = true)
        private String url;

        @JsonProperty(value = "redirectType")
        private Integer redirectType;
    }
}

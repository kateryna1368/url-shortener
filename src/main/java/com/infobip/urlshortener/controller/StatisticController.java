package com.infobip.urlshortener.controller;

import com.infobip.urlshortener.service.UrlShortenerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

import static com.infobip.urlshortener.controller.ConstantsHolder.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Controller
@RequestMapping(path = "/statistic")
public class StatisticController {
    @Autowired private UrlShortenerService urlShortenerService;

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public @ResponseBody Map<String, Integer> statistics(@AuthenticationPrincipal UserDetails userDetails) {
        return urlShortenerService.getStatisticByAccountId(userDetails.getUsername());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Map> internalErrorHandler(Exception ex) {
        Map<String, Object> responseBody = new HashMap<>();
        responseBody.put(SUCCESS, false);
        responseBody.put(DESCRIPTION, INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(responseBody, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}

create table urls (
    id INTEGER IDENTITY PRIMARY KEY,
    url character varying(1024) NOT NULL,
    shortUrl character varying(80) NOT NULL,
    accountId character varying(80) NOT NULL,
    redirectCount int not null,
    redirectCode int not null,
    UNIQUE (shortUrl)
);

create table accounts(
    id INTEGER IDENTITY PRIMARY KEY,
    accountId character varying(80) NOT NULL,
    password character varying(80) NOT NULL,
    UNIQUE (accountId)
);
# URL shortener

## Installing
To install and run application execute commands
```
mvn clean package
cd target/
java -jar url-shorter-1.0-war-exec.jar
```

## How to use

###Opening of accounts
Request
```
curl -H "Content-Type: application/json" -X POST -d '{"AccountId":"kateryna"}' http://localhost:8080/account
```
Responce
```
{"password":"vsD2NDwk","success":true,"description":"Your account is opened"}
```

###Registration of URLs
Request

```
curl --user kateryna:vsD2NDwk -H "Content-Type: application/json" -X POST -d '{"url":"https://www.infobip.com/","redirectType":"301"}' http://localhost:8080/register
```
Responce
```
{"success":true,"shortUrl":"http://localhost:8080/r/hvcPlY"}
```

###Get statistic
Request
```
curl --user kateryna:vsD2NDwk http://localhost:8080/statistic
```
Responce
```
{"https://infobip.com":2,"https://telegram.org":1}
```